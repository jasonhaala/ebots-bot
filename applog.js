'use strict';

const { createLogger, format, transports } = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');
const { combine, timestamp, label, printf } = format;

const logFormat = printf(info => {

    return `${info.timestamp} [${info.level.toUpperCase()}] :: ${info.message}`;
});

let minLevel = isLocalEnv ? 'debug' : 'info';

let levelNum = 0;
let logger = createLogger({
    levels: {
        emergency: levelNum,
        critical:  levelNum++,
        alert:     levelNum++,
        error:     levelNum++,
        warning:   levelNum++,
        info:      levelNum++,
        debug:     levelNum++
    },
    format: combine(
        //label({ label: 'HaalaMedia Bot' }),
        timestamp(),
        logFormat
    )
});

logger.exceptions.handle(
    new transports.File({ filename: 'logs/exceptions.log' })
);

if (isLocalEnv) {
    logger.add(new transports.Console({
        level: minLevel
    }));
} else {
    logger.add(new DailyRotateFile({
        level: 'error',
        filename: 'logs/errors-%DATE%.log',
        datePattern: 'YYYY-MM-DD-HH',
        zippedArchive: false,
        maxSize: '20m',
        maxFiles: '7d',
        handleExceptions: true
    }));

    logger.add(new DailyRotateFile({
        level: minLevel,
        filename: 'logs/combined-%DATE%.log',
        datePattern: 'YYYY-MM-DD-HH',
        zippedArchive: false,
        maxSize: '20m',
        maxFiles: '3d'
    }));
}

module.exports = logger;
