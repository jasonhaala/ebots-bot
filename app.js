'use strict';

require('crashreporter').configure({
    outDir: './logs', // (default: cwd)
    exitOnCrash: true, // have crash reporter use exit(1)
    maxCrashFile: 10, // Clean up old files. (default: 5 files),
    hiddenAttributes: [
        'execPath', 'requireCache', 'activeHandle'
    ],
});

process.on('unhandledRejection', (reason, promise) => {
    console.log('Unhandled Rejection at:', reason.stack || reason)
    // Recommended: send the information to a crash reporting service
})

require('dotenv').config({silent: true});

/*
 Modules
 */
const ebots = require('ebots-core');
const config = require('./bot/config');

/*
 Globals
 */
global.isLocalEnv = config.app.env.toLowerCase() === 'local' ? true : false;
global.isDebugEnabled = config.app.debug;
global.applog = require('./applog');

const channel = config.app.platform;

/*
 Build the bot controller
 */
ebots.controller(config, (error, controller) => {
    /*
     Spin up the server
     */
    ebots.server(controller, (error, server) => {
        /*
         Application routes
         */
        server.get('/', (_, res) => {
            res.render('./index', {
                pageTitle: 'E3OTS'
            });
        });

        server.get('/terms', (_, res) => {
            res.render('./terms', {
                pageTitle: 'Terms of Service'
            });
        });

        server.get('/privacy', (_, res) => {
            res.render('./privacy', {
                pageTitle: 'Privacy Policy'
            });
        });
    });


    /*
     Initialize this bot's dialogs & events
     */
    require('./bot')(controller);


    if (channel === 'facebook') {
        /*
        Run all Facebook bot settings
         */
        if (! isLocalEnv) {
            require('./bot/settings/facebook').setAll(controller);
        }
    }
});
