'use strict';

const ware = require('ware');

function CoreBot(configuration) {

    let corebot = {
        config: {},
        db: {},
        events: {},
    }

    /*
     List of events to exclude from conversations.
     Useful to exclude things like delivery confirmations, and other behind the scenes events
     */
    corebot.excludedEvents = [];

    /*
     Add a single event, or an array of events, to be excluded from the conversations
     */
    corebot.excludeFromConversations = function(events) {
        if (Array.isArray(events)) {
            for (let e = 0; e < events.length; e++) {
                corebot.excludedEvents.push(events[e]);
            }
        } else {
            corebot.excludedEvents.push(events);
        }
    };


    /* ================================================================================================================
     Middleware
     ================================================================================================================ */
    /*
    Define some middleware points where custom functions.
    Can plug into key points of thee bot's process
     */
    corebot.middleware = {
        spawn: ware(),
        ingest: ware(),
        normalize: ware(),
        categorize: ware(),
        receive: ware(),
        heard: ware(), // best place for heavy i/o because fewer messages
        triggered: ware(), // like heard, but for other events
        capture: ware(),
        format: ware(),
        send: ware(),
        conversationStart: ware(),
        conversationEnd: ware(),
    }

    corebot.ingest = function(bot, payload, source) {
        // keep an unmodified copy of the message
        payload.raw_message = clone(payload);

        payload._pipeline = {
            stage: 'ingest',
        };

        corebot.middleware.ingest.run(bot, payload, source, function(err, bot, payload, source) {
            if (err) {
                console.error('An error occurred in the ingest middleware: ', err);
                return;
            }

            corebot.normalize(bot, payload);
        });
    };

    corebot.normalize = function(bot, payload) {
        payload._pipeline.stage = 'normalize';

        corebot.middleware.normalize.run(bot, payload, function(err, bot, message) {
            if (err) {
                console.error('An error occurred in the normalize middleware: ', err);
                return;
            }

            if (! message.type) {
                message.type = 'message_received';
            }

            corebot.categorize(bot, message);
        });
    };

    corebot.categorize = function(bot, message) {
        message._pipeline.stage = 'categorize';

        corebot.middleware.categorize.run(bot, message, function(err, bot, message) {
            if (err) {
                console.error('An error occurred in the categorize middleware: ', err);
                return;
            }

            corebot.receiveMessage(bot, message);
        });
    };

    corebot.receiveMessage = function(bot, message) {
        message._pipeline.stage = 'receive';

        corebot.middleware.receive.run(bot, message, function(err, bot, message) {
            if (err) {
                console.error('An error occurred in the receive middleware: ', err);
                return;
            }

            corebot.debug('Message Received...');

            bot.findConversation(message, function(convo) {
                if (convo) {
                    convo.handle(message);
                } else {
                    corebot.trigger(message.type, [bot, message]);
                }
            });
        });
    };


    /* ================================================================================================================
     Dialog
     ================================================================================================================ */

    function Dialog(task, message) {

    }

}

module.exports = CoreBot;

