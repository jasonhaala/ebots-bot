'use strict';

const fbApi = require('ebots-core').facebookApi;
const config = require('../config');

module.exports = {

    setAll: function(controller) {
        /**
         * These settings will be ran on server load
         */
        setTimeout(function() {
            setGetStartedButton(controller);
        }, 1000);
        setTimeout(function() {
            setGreeting(controller);
        }, 1000);
        setTimeout(function() {
            setPersistentMenu(controller);
        }, 1000);
        setTimeout(function() {
            setPaymentSettings(controller);
        }, 1000);
        setTimeout(function() {
            setWhitelistedDomains(controller);
        }, 1000);
        setTimeout(function() {
            setHomeUrl(controller);
        }, 1000);
        setTimeout(function() {
            setAccountLinkingUrl(controller);
        }, 1000);
    },

    setGetStartedButton: setGetStartedButton,
    setGreeting: setGreeting,
    setPersistentMenu: setPersistentMenu,
    setPaymentSettings: setPaymentSettings,
    setWhitelistedDomains: setWhitelistedDomains,
    setHomeUrl: setHomeUrl,
    setAccountLinkingUrl: setAccountLinkingUrl,
};


/**
 * Set "Get Started" Button
 *
 * The postback value is set in: config.facebook
 */
function setGetStartedButton(controller) {
    if (! config.facebook.getStartedPostback) {
        controller.log.warning('Skipped setting Facebook "Get Started" button. Payload is not set in configs.');
    } else {
        fbApi.setGetStartedButton(controller, config.facebook.getStartedPostback);
    }
}


function setGreeting(controller) {
    let greeting = [
        {
            "locale": "default",
            "text": "Hello, {{user_first_name}}! Welcome to the Fantasy Bot demo."
        }
    ];

    fbApi.setGreeting(controller, greeting);
}


function setPersistentMenu(controller) {
    let menu = [
        {
            "locale": "default",
            "composer_input_disabled": false,
            "call_to_actions": [
                {
                    "title": "Show Main Menu",
                    "type": "postback",
                    "payload": "feature_carousel"
                },
                {
                    "title": "Get Started",
                    "type": "postback",
                    "payload": "GET_STARTED"
                },
                {
                    "type": "nested",
                    "title": "More info",
                    "call_to_actions": [
                        {
                            "title": "Manage subscription",
                            "type": "postback",
                            "payload": "manage_subscription"
                        },
                        {

                            "title": "Get a chatbot",
                            "type": "web_url",
                            "url": "https://haalamedia.com",
                            "webview_height_ratio": "full"
                        }
                    ]
                }
            ]
        }
    ];

    fbApi.setPersistentMenu(controller, menu);
}


/**
 * Set Whitelisted Domains
 *
 * This domains are set in: config.facebook
 */
function setWhitelistedDomains(controller) {
    if (! config.facebook.whitelistedDomains) {
        controller.log.info('Skipped setting Facebook whitelisted domains. The config property is missing.');
    } else {
        fbApi.setWhitelistedDomains(controller, config.facebook.whitelistedDomains);
    }
}


/**
 * Set Payment Settings
 *
 * This required public key is set in: config.facebook
 */
function setPaymentSettings(controller) {
    if (! (config.facebook.payments && config.facebook.payments.publicKey)) {
        controller.log.info('Skipped setting Facebook payments options. Public key is not set in configs.');
    } else {
        let settings = {
            privacy_url: config.app.url +'/privacy',
            public_key: config.facebook.payments.publicKey,
        }
        fbApi.setPaymentSettings(controller, settings);
    }
}


/**
 * Set Home URL
 *
 * This URL is set in: config.app
 */
function setHomeUrl(controller) {
    const whitelistedDomains = config.facebook.whitelistedDomains;
    if (! config.app.url) {
        controller.log.info('Skipped setting Facebook Home URL. Value is not set in configs.');
    } else if (Array.isArray(whitelistedDomains) && whitelistedDomains.includes(config.app.url)) {
        fbApi.setHomeUrl(controller, config.app.url);
    } else {
        controller.log.warning('Skipped setting Facebook Home URL. URL must be set in whitelisted domains. Check configs.');
    }
}


/**
 * Account Linking URL
 *
 * This URL is set in: config.facebook
 */
function setAccountLinkingUrl(controller) {
    if (! config.facebook.accountLinkingUrl) {
        controller.log.info('Skipped setting Facebook Account Linking URL. Value is not set in configs.');
    } else {
        fbApi.setAccountLinkingUrl(controller, config.facebook.accountLinkingUrl);
    }
}
