'use strict'

/*
 |--------------------------------------------------------------------------
 | Chatbot Analytics Services
 |--------------------------------------------------------------------------
 |
 */
module.exports = {

    /**
     * https://github.com/asopinka/chatbase-botkit
     */
    chatbase: {
        enabled: true,
        apiKey: process.env.ChatbaseApiKey || null,
    },

    /**
     * https://github.com/botmetrics/botkit-middleware-botmetrics
     */
    botmetrics: {
        enabled: false,
        apiKey: null,
        botId: null,
    },

    /**
     * https://www.dashbot.io/docs/facebook/botkit/
     */
    dashbot: {
        enabled: false,
        apiKey: process.env.DashbotApiKey || null,
    },

    botanalytics: {
        enabled: false,
    },

    yandex: {
        enabled: false,
     },

}
