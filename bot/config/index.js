'use strict'

const app = require('./app');
const analytics = require('./analytics');
const facebook = require('./facebook');
const nlp = require('./nlp');
const storage = require('./storage');
const cache = require('./cache');

module.exports = {
    app:       app,
    analytics: analytics,
    facebook:  facebook,
    nlp:       nlp,
    storage:   storage,
    cache:     cache
}

