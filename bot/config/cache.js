'use strict'

module.exports = {

    default: process.env.defaultCache || 'memory',

    ttl: 7200, // Expiration time (in seconds)

    file: {
        directory: process.env.cacheFileDirectory || 'cache',
        maxFileSize: process.env.cacheFileMaxSizeBytes ? parseInt(process.env.cacheFileMaxSizeBytes) : 10 * 1000 * 1000 // 10 MB
    },

    memory: {
        max: process.env.cacheMemoryMax || 500,
    },

    redis: {
        host: process.env.redisHost || '127.0.0.1',
        port: process.env.redisPort || '6379',
        db: process.env.redisDatabase || 0,
        pass: process.env.redisPassword || '',
    },

}
