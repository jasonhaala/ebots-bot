'use strict'

/*
 |--------------------------------------------------------------------------
 | Facebook Messenger Configuration
 |--------------------------------------------------------------------------
 |
 */
module.exports = {

    /**
     * Page Info
     */
    pageId: process.env.FacebookPageId || null,
    pageAccessToken: process.env.FacebookPageAccessToken || null,

    /**
     * App Info
     */
    appId: process.env.FacebookAppId || null,
    appSecret: process.env.FacebookAppSecret || null,
    verifyToken: process.env.FacebookVerifyToken || null,

    /**
     * Domains & Extensions
     */
    whitelistedDomains: null, //['ebots.pw', '...'],

    accountLinkingUrl: process.env.FacebookAcctLinkingUrl || null,

    /**
     * Payments
     */
    payments: null, /*{
        publicKey: process.env.FacebookPaymentsPublicKey || null,
        currency: 'USD',
        merchantName: 'HaalaMedia', // Business name
        requestedUserInfo: ['shipping_address', 'contact_name', 'contact_phone', 'contact_email']
    },*/

    /**
     * Messenger Settings
     */
    getStartedPostback: 'GET_STARTED',

}
