'use strict'

module.exports = {

    default: process.env.persistentStorage || null,

    enableMessageLogs: false,

    dbLogLevel: process.env.DbLogLevel || 'error', // <disabled|debug|warning|error|alert|emergency>

    mysql: {
        host: process.env.MySQLHost || '127.0.0.1',
        port: process.env.MySQLPort || '3306',
        db: process.env.MySQLDatabase || 'test',
        user: process.env.MySQLUsername || 'root',
        pass: process.env.MySQLPassword || '',
    },

    mongodb: {
        ssh: {
            host: process.env.MongoDbSSHHost || null,
            port: process.env.MongoDbSSHPort || null,
            user: process.env.MongoDbSSHUser || null,
            pass: process.env.MongoDbSSHPassword || null,
            privateKey: process.env.MongoDbSSHPrivateKeyName || null,
        },
        host: process.env.MongoDbHost || '127.0.0.1',
        port: process.env.MongoDbPort || '27017',
        db: process.env.MongoDbDatabase || null,
        user: process.env.MongoDbUsername || null,
        pass: process.env.MongoDbPassword || null,
        uniqueLocalPort: process.env.MongoDBUniquePort || 27018,
    },

    googleCloud: {
        projectId: process.env.GoogleCloudStorageProjectId || null,
    }

}
