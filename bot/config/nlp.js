'use strict'

/*
 |--------------------------------------------------------------------------
 | Natural Language Understanding (NLU) Services
 |--------------------------------------------------------------------------
 |
 */
module.exports = {

    dialogflow: {
        enabled: true,
        //projectId: process.env.DialogflowProjectId || null, // Project id is inside keyFile
        keyFile: process.env.DialogflowKeyFile || null
    },
    qnaMaker: {
        enabled: false,
        knowledgeBaseId: process.env.QnAKnowledgeBaseId || null,
        subscriptionKey: process.env.QnASubscriptionKey || null
    },
    LUIS: {
        enabled: false,
        endpointUrl: process.env.LuisEndpointUrl || null,
    }

}
