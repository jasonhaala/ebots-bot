
module.exports = {

    /*
     |--------------------------------------------------------------------------
     | Bot Tag
     |--------------------------------------------------------------------------
     |
     | The database can track multiple bots.
     | Set a unique tag that can be used to identify this specific bot.
     |
     */
    botTag: 'fantasy-demo',

    botId: process.env.BotId || null,



    /*
     |--------------------------------------------------------------------------
     | The messaging platform this bot will connect with
     |--------------------------------------------------------------------------
     |
     | Allowed values: web, facebook, slack, twilioSMS, twilioIPM, microsoftTeams, webexTeams, ciscoJabber
     |
     */
    platform: 'facebook',

    /*
     |--------------------------------------------------------------------------
     | Application Environment
     |--------------------------------------------------------------------------
     |
     | This value determines the "environment" your application is currently
     | running in. This may determine how you prefer to configure various
     | services your application utilizes.
     |
     */
    env: process.env.AppEnv || 'production',

    /*
     |--------------------------------------------------------------------------
     | Application Debug Mode
     |--------------------------------------------------------------------------
     |
     | When the application is in debug mode, detailed messages will be logged
     | to the console.
     |
     */
    debug: process.env.Debug && process.env.Debug.toLowerCase() === 'true' ? true : false,

    /*
     |--------------------------------------------------------------------------
     | Application URLs
     |--------------------------------------------------------------------------
     |
     */
    url: process.env.AppUrl || 'http://localhost',

    /*
     |--------------------------------------------------------------------------
     | Application Server Settings
     |--------------------------------------------------------------------------
     |
     */
    port: process.env.AppPort || '3000',

    viewEngine: 'ejs',


    /*
     |--------------------------------------------------------------------------
     | API Token for incoming requests
     |--------------------------------------------------------------------------
     |
     */
    apiToken: process.env.AppApiToken || null,

    /*
     |--------------------------------------------------------------------------
     | Google Cloud
     |--------------------------------------------------------------------------
     |
     */
    googleCloudProjectId: process.env.GoogleCloudProjectId || null,

}
