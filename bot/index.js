'use strict';

const fs = require('fs');
const path = require('path');

module.exports = function(controller) {

    /**
     * Event Handlers
     *
     * Uses: controller.on()
     */
    let eventsDir = path.join(process.cwd(), '/bot/events');
    fs.readdirSync(eventsDir).forEach(function(file) {
        if (file !== 'disabled') {
            require(eventsDir +'/'+file)(controller);
        }
    });


    /**
     * Dialogs
     *
     * Uses: controller.hears()
     */
    /*let dialogsDir = path.join(process.cwd(), '/bot/dialogs');
     fs.readdirSync(dialogsDir).forEach(function(file) {
        if (file !== 'disabled') {
            require(dialogsDir +'/'+file)(controller, factory);
        }
     });*/

    require('./dialog-router')(controller);

};
