'use strict';

/**
 * Fallback Handler
 *
 * This allows you to have control over how to respond to a user if no matching dialogs were found.
 *
 * @param controller
 * @param bot
 * @param message
 */
module.exports = function(controller, bot, message, user) {

    bot.reply(message, `Sorry, I haven't been trained to answer that yet. I'm still learning.`);
}
