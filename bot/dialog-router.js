'use strict';

const _ = require('lodash');
const path = require('path');
const fs = require('fs');
const ebots = require('ebots-core');

/*
Actions
 */
// Loop through all the action files
let actions = {};

let moduleFunctions;
let actionsDir = path.join(__dirname, './actions');
fs.readdirSync(actionsDir).forEach(function(file) {
    if (file.endsWith('.js')) {
        moduleFunctions = require(actionsDir +'/'+file);
        actions = Object.assign(actions, moduleFunctions);
    } else {
        if (file !== '_disabled') {
            let subfileActions = {};
            let actionsSubDir = path.join(__dirname, './actions/'+ file);
            fs.readdirSync(actionsSubDir).forEach(function(subfile) {
                if (subfile.endsWith('.js')) {
                    // Combine the exported functions with the objects of the same subdirectory
                    moduleFunctions = require(actionsSubDir +'/'+subfile);
                    subfileActions = Object.assign(subfileActions, moduleFunctions);
                }
            });

            // Attach the subdirectory actions
            actions[file] = subfileActions;
        }
    }
});


module.exports = function(controller) {

    /*
     Text Messages
     */
    controller.hears(['.*'], ['message_received'], (bot, message) => {

        /*
         Handle Stickers
         */
        if (message.sticker_id) {

            return controller.trigger('sticker_received', [bot, message]);
        }

        /*
         Handle Attachments
         */
        if (message.attachments && message.attachments[0].type) {
            let attachmentType = message.attachments[0].type;
            controller.log.debug('Received attachment type: '+attachmentType);
            switch(attachmentType) {
                case 'audio':
                case 'image':
                case 'video':
                case 'file':

                    return controller.trigger(attachmentType + '_received', [bot, message]);
            }
        }

        if (! message.text) {
            // @todo Handle any non-text responses

            return;
        }

        let msgText = message.text.toLowerCase();

        /*
        Set the app layer the bot should function on (base or admin)
         */
        // @todo Check if the user is logged into the admin layer
        /*let appLayer = 'base';
        if (msgText === 'admin') {
            // @todo Log the user into the admin layer, if authenticated.
            appLayer = 'admin';

            // @todo Set the "admin" app layer in cache for the user.
        }*/

        /*
         Check enabled NLP services
         */
        let nlpConfig = controller.ebots.config.nlp;
        if (nlpConfig.dialogflow.enabled) {
            /*
             Dialogflow
             */
            controller.log.debug('NLP: Sending request to Dialogflow...');
            const Dialogflow = ebots.dialogflowV2;

            // @todo If user is on the admin layer, use the admin keyfile

            let dialogflowConfig = {
                keyFilename: nlpConfig.dialogflow.keyFile
            }

            Dialogflow.process(message, dialogflowConfig, function(error, combinedMessage) {
                controller.log.debug(JSON.stringify(combinedMessage));
                controller.log.debug('Formatted NLP response message');
                controller.log.debug('==============================');

                if (combinedMessage && combinedMessage.nlp) {
                    /*
                     Process the NLP's messages or action
                     // @todo We may want the ability to BOTH send message(s) and trigger an action.
                     */
                    let responseMessages = _.result(combinedMessage, 'fulfillment.messages', null);
                    if (responseMessages && responseMessages.length > 1) {
                        /*
                         Send each of the messages from the NLP response
                         */
                        for (let i = 0; i < responseMessages.length; i ++) {
                            let response = responseMessages[i];
                            // Only send the response if the message isn't blank
                            if (response) {
                                /*
                                Send the reply message
                                 */
                                bot.reply(combinedMessage, response)
                            }
                        }
                    } else if (combinedMessage.action) {
                        /*
                         Trigger the action
                         */
                        triggerAction(combinedMessage.action, bot, combinedMessage);
                    } else {
                        controller.log.debug('No NLP response or action to handle.');

                        triggerFallback(bot, message);
                    }
                } else {
                    triggerFallback(bot, message);
                }
            });
        } else {
            triggerFallback(bot, message);
        }
    });


    /*
     Postbacks

     Tap Tracking: message payload will be formatted as: "tap:[RouteId]:[targetAction]"
     */
    controller.hears(['.*'], ['postback', 'facebook_postback'], (bot, message) => {

        let action;
        let payload = message.text;
        console.log('Payload: '+ payload);
        if (payload.startsWith('tap:')) {

            // Split the array to get each usable piece of the tracking data
            let splitPayload = payload.split(':');

            // Get the route id for analytics
            let routeId = splitPayload[1];
            controller.trigger('track_tap', [routeId]);

            // The action will always be the last element of the split array
            action = splitPayload.pop();
        } else {
            action = payload;
        }
        // @todo Setup handling for tap router
        // "tap:[RouteId]:[targetAction]"

        triggerAction(message.text, bot, message);
    });


    /*
     NLP Actions
     */
    function triggerAction(action, bot, message) {

        controller.log.debug('Triggering action: '+ action);

        let user = bot.userProfile;

        action = action.toLowerCase().replace(/\s+/g, ''); // lowercase & remove all spaces

        /*
        Authenticate any "Admin" actions
         */
        if (action.startsWith('admin.')) {
            if (! (user && user.isAdmin)) {
                // User was not found, or is unauthenticated.
                // To prevent the knowledge of any admin features existing to unauthenticated users,
                // don't return a response but log the attempted access.
                console.log('Unauthenticated user attempting to access admin features');
                // @todo Log the attempted access to the database, and/or send an alert.

                return;
            }
        }

        try {
            // Attempt to fire the action
            _.get(actions, action, null)(bot, message, user);
        } catch(error) {
            if (error) {
                // Action does not exist, or the function contains an error
                console.log(error)
                // @todo Log the error to the db

                // Trigger the fallback response handler
                controller.trigger('fallback', [bot, message]);
            }
        }
    }

    function triggerFallback(bot, message) {
        controller.trigger('fallback', [bot, message]);
    }

}
