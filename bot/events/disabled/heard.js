'use strict';

/**
 * A trigger defined with controller.hears() was fired.
 */
module.exports = function(controller) {

    controller.on('heard_trigger', (bot, triggers, message) => {
        controller.log.debug('Triggered Event: (heard_trigger)...');
    });

}
