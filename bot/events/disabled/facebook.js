/**
 * Facebook-Specific Events
 *
 * https://botkit.ai/docs/readme-facebook.html#event-list
 */

module.exports = function(controller) {

    /**
     * A confirmation from Facebook that a message has been received
     */
    controller.on('message_delivered', (bot, message) => {
        controller.log.debug('Event: (message_delivered) Message delivered.');
    });

    /**
     * A confirmation from Facebook that a message has been read
     */
    controller.on('message_read', (bot, message) => {
        controller.log.debug('Event: (message_read) Message read.');
    });

    /**
     * If enabled in Facebook, an "echo" of any message sent by the bot
     */
    /*controller.on('message_echo', (bot, message) => {
        controller.log.debug('Event: (message_echo) Message echo.');
    });*/

    /**
     * A Facebook user clicked a button in an attachment and triggered a webhook postback
     */
    controller.on('facebook_postback', (bot, message) => {
        controller.log.debug('Event: (facebook_postback) Facebook postback received.');
    });

    /**
     * A user has clicked on Facebook's m.me URL with a referral param
     *
     * https://developers.facebook.com/docs/messenger-platform/discovery/m-me-links
     */
    controller.on('facebook_referral', (bot, message) => {
        controller.log.debug('Event: (facebook_referral) Facebook conversation started using m.me referral parameter.');
    });

    /**
     * A user has clicked the Facebook Send-to-Messenger plugin
     *
     * https://developers.facebook.com/docs/messenger-platform/discovery/send-to-messenger-plugin
     */
    controller.on('facebook_optin', (bot, message) => {
        controller.log.debug('Event: (facebook_optin) Facebook Send-to-Messenger button clicked.');
    });


    /**
     * This callback will occur when a message has been sent to your page,
     * but your application is not the current thread owner.
     */
    /*controller.on('standby', (bot, message) => {
        controller.log.debug('Event: (standby) Facebook standby message sent.');
    });*/

    /**
     * A user has started the account linking
     */
    /*controller.on('facebook_account_linking', (bot, message) => {
        controller.log.debug('Event: (facebook_account_linking) Account linking started for Facebook user.');
    });*/

    /**
     * This callback will occur when a page admin changes the role of your application.
     */
    /*controller.on('facebook_app_roles', (bot, message) => {
        controller.log.debug('Event: (facebook_app_roles) Facebook page admin changed role of application.');
    });*/

    /**
     * This callback will occur when thread ownership for a user has been passed to your application.
     */
    /*controller.on('facebook_receive_thread_control', (bot, message) => {
        controller.log.debug('Event: (facebook_receive_thread_control) Facebook thread ownership for user has been passed to bot.');
    });*/

    /**
     * This callback will occur when thread ownership for a user has been taken away from your application.
     */
    /*controller.on('facebook_lose_thread_control', (bot, message) => {
        controller.log.debug('Event: (facebook_lose_thread_control) Facebook thread ownership for user has been taken away from bot.');
    });*/


    /*controller.on('channel_join', (bot, event) => {
        controller.log.debug('Event: () Channel join event');
        controller.log.debug(event);
        bot.reply(event, 'Welcome to the channel!');
    });*/

}
