'use strict';

/**
 * A trigger used to alert admins of critical errors or notifications
 */
module.exports = function(controller) {

    controller.on('admin_alert', (level, message) => {
        controller.log.debug('An admin alert has been triggered.');
    });

}
