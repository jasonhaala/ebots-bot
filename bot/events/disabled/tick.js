'use strict';

/**
 * The event loop has ticked.
 * Handler does not receive any parameters.
 */
module.exports = function(controller) {

    controller.on('tick', () => {
        controller.log.debug('Triggered Event: (tick)...');
    });

}
