'use strict';

/**
 * A conversation has ended.
 */
module.exports = function(controller) {

    controller.on('conversation_ended', function(bot, convo) {
        controller.log.debug(`Triggered Event: (conversation_ended). A conversation with ${convo.context.user} has ended.`);
    });

}
