'use strict';

/**
 * One of the bot's NLP services matched the message
 *
 * A normalized message object will be returned by all the different services,
 * allowing a response to be handled the same way
 */
module.exports = function(controller) {

    controller.on('nlu_matched', (bot, message) => {
        controller.log.debug('Triggered Event: (nlu_matched)...');
        //
    });

}
