'use strict';

const path = require('path');

/**
 * In a 1:1 platform, indicates an incoming message from user has been received
 */
module.exports = function(controller) {

    controller.on('message_received', (bot, message) => {
        controller.log.debug('Triggered Event: (message_received)...');
    });

}
