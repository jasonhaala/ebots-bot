/**
 * Broadcast Events
 */

module.exports = function(controller) {

    controller.on('broadcast_sent', (bot, message) => {
        console.log('Broadcast message sent.');
    });

}
