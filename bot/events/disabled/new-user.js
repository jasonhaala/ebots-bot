'use strict';

module.exports = function(controller) {

    controller.on('new_user', (bot, message, userProfile) => {
        let identity = 'unidentified';

        if (userProfile.firstName) {
            identity = userProfile.firstName
        }

        if (userProfile.lastName) {
            identity += ' '+userProfile.lastName
        }

        controller.log.debug(`Triggered Event: (new_user). ${identity} has connected.`);
    });

}
