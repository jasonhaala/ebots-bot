'use strict';

/**
 * Postback Received
 */
module.exports = function(controller) {

    controller.on('postback', (bot, message) => {
        controller.log.debug('Triggered Event: (postback)...');
    });

}
