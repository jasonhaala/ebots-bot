'use strict';

module.exports = function(controller) {

    controller.on('new_connection', (bot, message) => {
        controller.log.debug(`Triggered Event: (new_connection)...`);
    });

}
