'use strict';

/**
 * New Session
 */
module.exports = function(controller) {

    controller.on('new_session', (bot, message, userProfile) => {
        let identity = userProfile.firstName || 'An unidentified user';
        controller.log.debug(`Triggered Event: (new_session). ${identity}) has started a new session with the bot.`);
    });

}
