'use strict';

/**
 * A conversation has started.
 */
module.exports = function(controller) {

    controller.on('conversation_started', function(bot, convo) {
        controller.log.debug(`Triggered Event: (conversation_started). A conversation with ${convo.context.user} has started.`);
    });

}
