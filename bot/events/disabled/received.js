/**
 * Received Message Type Events
 */

module.exports = function(controller) {

    controller.on('sticker_received', (bot, message) => {
        console.log('Sticker received.');
    });

    controller.on('image_received', (bot, message) => {
        console.log('Image received.');
    });

    controller.on('file_received', (bot, message) => {
        console.log('File attachment received.');
    });

    controller.on('audio_received', (bot, message) => {
        console.log('Audio attachment received.');
    });

    controller.on('video_received', (bot, message) => {
        console.log('Video attachment received.');
    });

}
