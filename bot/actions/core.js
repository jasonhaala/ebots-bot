'use strict';

let msg;

module.exports = {

    get_started: (bot, message, user) => {
        console.log('=====================================')
        console.log('get_started');
        console.log('=====================================')

        let factory = bot.botkit.factory;

        return new Promise(function(resolve, reject) {
            msg = new factory.Message()
                .attachment(new factory.Image()
                    //.url('https://storage.googleapis.com/tanyatate-d12a1.appspot.com/59f95269e0800e5a08b99ff908cc4ea4.jpg')
                    // .reusable()
                    .attachmentId('531504290607399')
                );

            setTimeout(function() {
                bot.reply(message, msg);
                resolve();
            }, 1500);
        }).then(function() {
            return new Promise(function (resolve, reject) {
                msg = `Hi, `+ (user.firstName || 'there') +`! Welcome to my official messenger experience!\r\n\r\nThis is the best place to stay up-to-date with everything, along with getting to see more personal side. 💋`;

                setTimeout(function() {
                    bot.reply(message, msg);
                    resolve();
                }, 1500);
            })
        }).then(function() {
            return new Promise(function (resolve, reject) {
                msg = new factory.Message()
                    .text(`Are you ready to see more?\r\n\r\nJust tap that button below 😉`)
                    .quickReplies([
                        factory.QuickReply.text(`Yes, show me more!`, 'get_started_continue')
                    ]);

                bot.startTyping(message, function () {
                    setTimeout(function() {
                        bot.reply(message, msg);

                        resolve();
                    }, 3000);
                });
            });
        });
    },

    greeting: (bot, message, user) => {
        bot.reply(message, 'Hey there'+ (user.firstName ? ', '+ user.firstName : '') +'!');
    },

}
