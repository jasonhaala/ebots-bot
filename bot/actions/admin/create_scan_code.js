'use strict';

let msg;

module.exports = {

    create_scan_code: (bot, message, user) => {
        // @todo Get platform to redirect action
        // Check for NLP parameter passed back.
        console.log(message);
        let platform = null;

        // If no parameter exists, use the platform the bot is connected to
        if (! platform) {
            platform = bot.botkit.platform;
        }

        bot.startConversation(message, function(err, convo) {

            convo.transitionTo('campaign', `I can help you create a Facebook Messenger Scan Code.`);

            convo.addQuestion({ text: `Enter a campaign id you would like to use. (Example: "business-card" or "may-promo-offer-flyer")` }, function(response, convo) {
                // ref tag has been collected...
                convo.setVar('refTag', response.text);
                convo.gotoThread('verify');
            }, { key: 'refTag' }, 'campaign');

            convo.addQuestion(
                {
                    text: 'Just to verify, is this the campaign id you want to use?\r\n\r\n {{vars.refTag}}',
                    quick_replies: [
                        {
                            title: 'Yes',
                            payload: 'yes',
                        },
                        {
                            title: 'No',
                            payload: 'no',
                        }
                    ]
                },
                [
                    {
                        pattern: 'yes',
                        callback: function(response, convo) {
                            let refTag = convo.extractResponse('refTag');
                            if (! refTag) {
                                // @todo Handle missing tag
                                console.log('Missing ref tag...');
                                convo.next();
                                return;
                            }
                            bot.botkit.api.messenger_profile.get_messenger_code(2000, function (error, scanCodeUrl) {
                                if (scanCodeUrl) {
                                    bot.botkit.ebots.db.scancodes.save({
                                        platform: platform,
                                        refTag: refTag,
                                        imageUrl: scanCodeUrl
                                    }, function(error, insert) {
                                        if (insert) {
                                            convo.say('Your scan code has been created! Below is the link to your scan code image.\r\n\r\nDownload the image to display it online or add to any printed materials.');
                                            convo.say(scanCodeUrl);
                                            convo.next();
                                        } else {
                                            if (error) {
                                                // @todo Handle errors.
                                                console.log(error);
                                            }

                                            convo.next();
                                        }
                                    });
                                } else {
                                    if (error) {
                                        // @todo Handle error.
                                        console.log(error);
                                    }

                                    convo.next();
                                }
                            }, refTag);

                        }
                    },
                    {
                        pattern: 'no',
                        callback: function(response, convo) {
                            convo.gotoThread('campaign');
                            convo.next();
                        }
                    },
                ],
                {}, 'verify');
        });
    },

}
