'use strict';

let msg;

module.exports = {

    main: (bot, message, user) => {
        let factory = bot.botkit.factory;

        msg = new factory.Message()
            .attachment(new factory.GenericTemplate('square')
                .elements([
                    new factory.GenericCard()
                        .title('Analytics')
                        .subtitle('Get analytics reports from Google Chatbase')
                        .image('https://cdn-images-1.medium.com/max/1600/1*5oPC3qgjZY8cq8OBqnuhYA.png')
                        .buttons([
                            factory.Button.postback(`Analytics Summary`, 'admin.report_analytics_summary'),
                            factory.Button.openUrl('https://chatbase.com/overview', `Chatbase Analytics`)
                        ]),
                    new factory.GenericCard()
                        .title('User Summary')
                        .subtitle('Display a summary of users connected to your bot.')
                        .image('https://static.adweek.com/adweek.com-prod/wp-content/uploads/sites/2/2015/06/twitter-profiles.jpg')
                        .buttons([
                            factory.Button.postback(`See User Summary`, 'admin.report_user_summary')
                        ]),
                    new factory.GenericCard()
                        .title('Facebook Scan Codes')
                        .subtitle('Get your Facebook Messenger Scan Codes.')
                        .image('https://www.canvasmagazine.com.bd/wp-content/uploads/2018/06/massanger.png')
                        .buttons([
                            factory.Button.postback(`Create Scan Code`, 'admin.facebook_create_scan_code')
                        ]),
                    new factory.GenericCard()
                        .title('Facebook Settings')
                        .subtitle('Make changes to Facebook settings.')
                        .image('https://www.canvasmagazine.com.bd/wp-content/uploads/2018/06/massanger.png')
                        .buttons([
                            factory.Button.postback(`View Options`, 'admin.facebook_settings')
                        ]),
                    new factory.GenericCard()
                        .title('Media Upload')
                        .subtitle('Upload images, videos or files to be used by the bot.')
                        .image('https://www.canvasmagazine.com.bd/wp-content/uploads/2018/06/massanger.png')
                        .buttons([
                            factory.Button.postback(`Upload an Attachment`, 'admin.attachment_upload')
                        ])
                ]));

        bot.reply(message, msg);
    },

    status: (bot, message, user) => {
        let totalUsers = 0;
        let activeUsers = 0;
        let subscribedUsers = 0;

        new Promise(function(resolve, reject) {
            bot.botkit.ebots.db.users.all(function(err, users) {
                if (err) {
                    console.error(err);
                    resolve(false);
                } else {
                    totalUsers = users.length;
                    for (let i = 0; i < totalUsers; i++) {
                        console.log(i +' of '+ totalUsers);
                        if (users.isActive) {
                            activeUsers++;
                        }
                        if (users.getUpdates) {
                            subscribedUsers++;
                        }

                        if (i === totalUsers - 1) {
                            resolve(true);
                        }
                    }
                }
            });
        }).then(function(usersCounted) {
            let uptime = formatUptime(process.uptime());
            let convoCount = bot.botkit.convoCount;

            if (! usersCounted) {
                totalUsers = 'unavailable';
                activeUsers = 'unavailable';
                subscribedUsers = 'unavailable';
            }

            msg = `Here's the current status of your bot:
            \r\n\r\nOnline for: ${uptime}
            \r\nProcessed conversations: ${convoCount}
            \r\n\r\nTotal users: ${totalUsers}
            \r\nActive users: ${activeUsers}
            \r\nSubscribed users: ${subscribedUsers}`;

            bot.reply(message, msg);
        }).catch(function(error) {
            console.error(error);
        });
    },

    facebook_settings: (bot, message, user) => {
        msg = `Facebook settings module.`;

        bot.reply(message, msg);
    },

    refresh_menu: (bot, message, user) => {
        //fbSettings.setPersistentMenu(bot.botkit);

        setTimeout(function() {
            msg = `I refreshed the Facebook Messenger menu for you.\r\n\r\nThere may be a delay before Facebook's servers update the changes.`;
            bot.reply(message, msg);
        }, 1500);
    },

}

/**
 * Utility function to format uptime
**/
function formatUptime(uptime) {
    var unit = 'second';
    if (uptime > 60) {
        uptime = uptime / 60;
        unit = 'minute';
    }
    if (uptime > 60) {
        uptime = uptime / 60;
        unit = 'hour';
    }
    if (uptime != 1) {
        unit = unit + 's';
    }

    uptime = parseInt(uptime) + ' ' + unit;

    return uptime;
}
