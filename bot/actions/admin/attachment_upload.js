'use strict';

const path = require('path');
const downloader = require('image-downloader');
const http = require('http');
const https = require('https');
const fileType = require('file-type');
const uuidv1 = require('uuid/v1');
const _ = require('lodash');

let msg;

module.exports = {

    attachment_upload: (bot, message, user) => {
        let factory = bot.botkit.factory;
        let platform = bot.botkit.platform || 'facebook';

        bot.startConversation(message, function(err, convo) {
            convo.addQuestion({ text: `Send me a file and I will upload it for you.` }, function(response, convo) {

                let attachmentUrl; // The url of the original attachment
                let attachmentType; // The type of attachment (image, etc)
                let attachmentId; // Facebook's reusable attachment id
                let fileUrl; // The final destination for the saved attachment

                /*
                Get the attachment URL
                 */
                if (response.attachments && response.attachments[0].payload) {
                    let attachment = response.attachments[0];
                    attachmentType = attachment.type;
                    attachmentUrl = attachment.payload.url;
                } else if (response.text && response.text.startsWith('http')) {
                    // Grab the typed URL
                    attachmentUrl = response.text;
                } else {
                    convo.transitionTo('retry', `I didn't receive an attachment file.`);
                }

                if (attachmentUrl) {
                    /*
                     Determine attachment type
                     */
                    new Promise(function (resolve, reject) {
                        if (attachmentType) {
                            resolve();
                        } else {
                            // Read the file
                            const protocol = attachmentUrl.startsWith('https:') ? https : http;
                            protocol.get(attachmentUrl, res => {
                                res.once('data', chunk => {
                                    res.destroy(); // Destroy the temp file data
                                    const fileTypeData = fileType(chunk); //=> {ext: 'gif', mime: 'image/gif'}
                                    //let fileExt = fileTypeData.ext;
                                    let mimeType = fileTypeData.mime;
                                    if (mimeType.startsWith('image')) {
                                        attachmentType = 'image';
                                    }
                                    resolve();
                                });
                            });
                        }
                    }).then(function() {
                        /*
                         Create the attachment object to upload
                         Only allow specific file types
                         */
                        return new Promise(function (resolve, reject) {
                            console.log('Attachment type: '+ attachmentType || null);
                            let upload = null;
                            if (! attachmentType) {
                                throw new Error(`Sorry... I'm not able to use this type of attachment.`);
                            } else if (attachmentType === 'image') {
                                upload = new factory.Image()
                                .url(attachmentUrl)
                                .reusable()
                            } else {

                            }

                            if (upload) {
                                convo.setVar('attachmentType', attachmentType);
                            } else {
                                throw new Error(`Sorry... ${attachmentType} attachments aren't supported yet.`);
                            }

                            resolve(upload);
                        });
                    }).then(function(upload) {
                        return new Promise(function (resolve, reject) {
                            /*
                             Download attachment to the local server
                             */
                            console.log('Downloading attachment to server...');
                            // Set the extension of attachment file
                            // Strip any url arguments that were in the url
                            let fileExt = path.extname(attachmentUrl).split('?')[0];
                            let publicDir = 'uploads'; // directory in public assets
                            let newFilename = uuidv1() + fileExt; // unique filename for attachment
                            let fileDest = path.join(process.cwd(), `/public/${publicDir}/${newFilename}`);

                            // Set the app's url, without a trailing slash
                            let appUrl = bot.botkit.ebots.config.app.url.replace(/\/$/, '');

                            // Download the file
                            downloader.image({
                                url: attachmentUrl,
                                dest: fileDest
                            }).then(({filename, image}) => {
                                fileUrl = `${appUrl}/${publicDir}/${newFilename}`;
                                resolve(upload);
                            }).catch((err) => {
                                console.error(err);
                                throw new Error(`I was unable to download the attachment.`);
                            });
                        });
                    }).then(function(upload) {
                        /*
                         Upload file to Facebook
                         */
                        console.log(`Uploading attachment to ${platform}...`);
                        return new Promise(function (resolve, reject) {
                            bot.botkit.api.attachment_upload.upload(upload, function (err, fbAttachmentId) {
                                if (fbAttachmentId) {
                                    attachmentId = fbAttachmentId;
                                    resolve(true);
                                } else {
                                    if (err) {
                                        // @todo Handle error
                                        console.log(error);
                                    }
                                    resolve(false);
                                }
                            });
                        });
                    }).then(function(uploadedToPlatform) {
                        if (! uploadedToPlatform) {
                            throw new Error(`Sorry, there was a problem uploading the file to ${platform}.`);
                        } else {
                            /*
                             Upload file to Google Cloud storage
                             */
                            console.log('Uploading attachment file to Google Cloud storage...');
                            return new Promise(function (resolve, reject) {
                                if (bot.botkit.googleCloud) {
                                    bot.botkit.googleCloud.storage.upload(file, config, function(error, upload) {
                                        if (upload) {
                                            resolve(true);
                                        } else {
                                            if (error) {
                                                console.error(error);
                                            }
                                            resolve(false);
                                        }
                                    });
                                } else {
                                    resolve(false);
                                }
                            });
                        }
                    }).then(function(savedToCloud) {
                        /*
                         Save attachment to the database
                         Will not be saved if non-production and not saved to cloud
                         */
                        return new Promise(function (resolve, reject) {
                            if (! (bot.botkit.isProduction && savedToCloud)) {
                                console.log('Skipping database...');
                                resolve(false);
                            } else {
                                let fileData = {
                                    url: fileUrl,
                                    facebookAttachmentId: attachmentId,
                                    savedToCloud: savedToCloud ? true : false
                                };

                                console.log('Uploading attachment to database...');
                                bot.botkit.ebots.db.attachments.save(fileData, function(error, document) {
                                    // Return the attachment id
                                    if (document) {
                                        if (savedToCloud) {
                                            // @todo Remove file from local server
                                        }
                                        resolve(true);
                                    } else {
                                        if (error) {
                                            console.log(error);
                                        }
                                        resolve(false);
                                    }
                                });
                            }
                        });
                    }).then(function(savedToDatabase) {
                        /*
                         Continue the conversation
                         */
                        console.log('Continuing conversation...');
                        if (savedToDatabase) {
                            convo.say(`Your file was uploaded and saved!`);
                        } else {
                            convo.say(`The file was uploaded to ${platform}, but wasn't saved to the database.\r\nKeep this URL for future reference: ${fileUrl}`);
                        }
                        convo.say(`The ${platform} attachment id is: ${attachmentId}.\r\n\r\nUse this attachment id to display the file in your bot or broadcast messages.`);
                        convo.next();
                    }).catch(function (error) {
                        console.error(error);
                        let errorMsg = _.isString(error) ? error : `Sorry...there was a problem uploading the file.`;
                        convo.transitionTo('retry', errorMsg);
                    });
                }
            }, {}, 'default');

            convo.addMessage({text: `OK. Exiting...`}, 'exit');

            convo.addQuestion(
                {
                    text: `Do you want to retry?`,
                    quick_replies: [
                        {
                            title: 'Yes',
                            payload: 'yes',
                        },
                        {
                            title: 'No',
                            payload: 'no',
                        }
                    ]
                },
                [
                    {
                        pattern: 'yes',
                        callback: function(response, convo) {
                            convo.gotoThread('default');
                            convo.next();
                        }
                    },
                    {
                        pattern: 'no',
                        callback: function(response, convo) {
                            convo.say(`OK. Exiting...`);
                            convo.next();
                        }
                    },
                ],
                {}, 'retry');
        });

    },

}
