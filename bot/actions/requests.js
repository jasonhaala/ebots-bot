'use strict';

module.exports = {

    sendapic: (bot, message, user) => {
        let factory = bot.botkit.factory;

        let msg = new factory.Message()
            .attachment(new factory.Image()
                .attachmentId('731082877224021')
            );

        bot.reply(message, msg);
    }

}
