'use strict';

let msg;

module.exports = {

    subscribe: (bot, message, user) => {
        console.log('=====================================')
        console.log('subscribe');
        console.log('=====================================')

        // Check if the user is already unsubscribed
        if (user.getUpdates) {
            bot.reply(message, `You already have updates turned on 💋`);
        } else {
            // Check if th user has been a subscriber in the past
            if (user.pastSubscriber) {

                return renewSubscription(bot, message, user);
            } else {

                return newSubscription(bot, message, user);
            }
        }
    },

    unsubscribe: (bot, message, user) => {
        console.log('=====================================')
        console.log('unsubscribe');
        console.log('=====================================')

        let factory = bot.botkit.factory;

        // Check if the user has already been unsubscribed
        if (! user.getUpdates) {
            bot.reply(message, `You already have updates turned off.`);
        } else {
            // Unsubscribe the user
            updateSubscription('unsubscribe', bot, user, function(error, updated) {

                // @todo Handle message if error or if subscription wasn't updated
                msg = new factory.Message()
                    .attachment(new factory.ButtonTemplate()
                        .text(`I respect your wishes${user.firstName ? ', '+user.firstName : ''}. I won't send any future updates.\r\n\r\nIf you ever decide you want to receive updates from me again, you can type: *update me* at any time.\r\n\r\n...or just press the "Turn On Updates" button below:`)
                        .buttons([
                            factory.Button.postback('Turn On Updates', 'subscribe')
                        ]));
                bot.reply(message, msg);
            });
        }
    },

    subscription_invite: (bot, message, user) => {
        console.log('=====================================')
        console.log('subscription_invite');
        console.log('=====================================')

        let factory = bot.botkit.factory;

        msg = new factory.Message()
            .text('Before we get started...\r\nWould you like to get exclusive updates from me here on messenger?')
            .quickReplies([
                factory.QuickReply.text(`Yes, please!`, 'subscribe'),
                factory.QuickReply.text(`I'm not interested`, 'decline_subscription'),
            ]);

        bot.reply(message, msg);
    },

    decline_subscription: (bot, message, user) => {
        updateSubscription('unsubscribe', bot, user, function(error, updated) {
            if (error) {
                // @todo Handle message if error or if subscription wasn't updated
            }

            let msg = `No problem${user.firstName ? ', '+user.firstName : ''} 🙂\r\n\r\nIf you ever decide you want to get updates from me, just type: *update me*`

            bot.reply(message, msg);
        });
    },

    manage_subscription: (bot, message, user) => {
        let factory = bot.botkit.factory;

        let imgUrl;
        let imgAttachmentId;
        let btnText;
        let btnTitle;
        let btnPostback;

        new Promise(function (resolve, reject) {
            if (user.getUpdates) {
                bot.reply(message, `You are currently getting updates from Tanya ❤`);

                //imgUrl = 'https://media.giphy.com/media/OJVQOfitCNNkI/giphy.gif';
                imgAttachmentId = '313602462709620';
                btnText = `To turn off updates, press the button below.`;
                btnTitle = 'Turn off updates';
                btnPostback = 'unsubscribe';
            } else {
                bot.reply(message, `You have chosen not to get updates from Tanya 🙁`);

                //imgUrl = 'https://media.giphy.com/media/102nX8tlIYDkAM/giphy.gif';
                imgAttachmentId = '375808119619455';
                btnText = `Tap the button below to turn on updates.`;
                btnTitle = 'Update Me!';
                btnPostback = 'subscribe';
            }
            resolve();
        }).then(function(result) {
            return new Promise(function (resolve, reject) {
                msg = new factory.Message()
                    .attachment(new factory.Image()
                        .attachmentId(imgAttachmentId)
                    );

                bot.reply(message, msg);

                setTimeout(function() {
                    resolve();
                }, 1500);
            });
        }).then(function(result) {
            return new Promise(function (resolve, reject) {
                msg = new factory.Message()
                    .attachment(new factory.ButtonTemplate()
                        .text(btnText)
                        .buttons([
                            factory.Button.postback(btnTitle, btnPostback)
                        ]));

                bot.reply(message, msg);
                resolve();
            });
        });
    },

}


function newSubscription(bot, message, user) {
    updateSubscription('subscribe', bot, user, function(error, updated) {
        // @todo Handle message if error or if subscription wasn't updated

        new Promise(function (resolve, reject) {
            msg = `Great choice${user.firstName ? ', '+user.firstName : ''}! 💋`;

            bot.reply(message, msg);
            resolve();
        }).then(function(result) {
            return new Promise(function (resolve, reject) {
                msg = `You will be the first to hear about my latest videos, get private photos, find out about upcoming events and much more.\r\n\r\nYou will also get access to special live events not available anywhere else! 💗`;

                bot.reply(message, msg);
                resolve();
            });
        });
    });
}

function renewSubscription(bot, message, user) {
    console.log('==================================')
    console.log('renewSubscription')
    console.log('==================================')

    updateSubscription('subscribe', bot, user, function(error, updated) {
        // @todo Handle message if error or if subscription wasn't updated
        console.log(message);
        msg = `I'm happy to have you back as a subscriber${user.firstName ? ', '+user.firstName : ''} 💋`;

        bot.reply(message, msg);
    });
}

function updateSubscription(action, bot, user, callback) {
    console.log('==================================')
    console.log('updateSubscription')
    console.log('==================================')

    console.log(user);
    if (action === 'subscribe') {
        user.getUpdates = true;

        // Set a field showing the user was once a subscriber
        // This will allow a more personalized response if they unsubscribe or resubscribe in the future
        user.pastSubscriber = true;
    } else {
        user.getUpdates = false;
    }

    // Update the user
    bot.botkit.ebots.db.users.save(user, function(error, savedUser) {
        console.log('.....');
        console.log(savedUser);
        let updated = error || ! savedUser ? false : true;
        console.log('Updated: '+updated)
        if (updated) {
            // Re-cache profile
            console.log('Caching user profile')
            let cacheTTL = bot.botkit.ebots.config.cache.ttl;
            bot.botkit.cache.set(user.userId, JSON.stringify(savedUser), 'EX', cacheTTL);
        }

        callback(error, updated);
    });
}
