'use strict';

const factory = require('ebots-facebook-factory');

let msg;

module.exports = {

    no_push_reply: (bot, message, user) => {
        msg = {
            text: "This message will not have any push notification on a mobile phone",
            notification_type: "NO_PUSH"
        }
        bot.reply(message, msg)
    },

    silent_push_reply: (bot, message, user) => {
        msg = {
            text: "This message will have a push notification on a mobile phone, but no sound notification",
            notification_type: "SILENT_PUSH"
        }
        bot.reply(message, msg);
    },

    echo: (bot, message, user) => {
        bot.startConversation(message, function(err, convo) {
            convo.ask('How are you?',function(response, convo) {
                convo.say('You said: '+ response.text);
                convo.next();
            });
        });
    },

    question: (bot, message, user) => {
        bot.startConversation(message, function(err, convo) {
            convo.addQuestion('Are you ready to see more?', [
                {
                    pattern: bot.utterances.yes,
                    callback: function(response, convo) {
                        convo.say('Great! I will continue...');
                        // do something else...
                        convo.next();

                    }
                },
                {
                    pattern: bot.utterances.no,
                    callback: function(response, convo) {
                        convo.say('Perhaps later.');
                        // do something else...
                        convo.next();
                    }
                },
                {
                    default: true,
                    callback: function(response, convo) {
                        // just repeat the question
                        convo.repeat();
                        convo.next();
                    }
                }
            ],{},'default');
        });
    },

    structured_convo: (bot, message, user) => {
        bot.startConversation(message, function(err, convo) {
            let msg = new factory.Message()
                .attachment(new factory.GenericTemplate()
                    .elements([
                        new factory.GenericCard()
                            .title('Title')
                            .subtitle('Subtitle')
                            .image('https://i.imgur.com/W4ycGD1.jpg')
                            .buttons([
                                factory.Button.postback('About Me', 'about_me'),
                            ]),
                    ])
                ).strip();

            convo.ask(msg, function(response, convo) {
                // whoa, I got the postback payload as a response to my convo.ask!
                convo.say(`I got the postback payload of your button click`);
                convo.next();
            });
        });
    },

    buttons: (bot, message, user) => {
        msg = new factory.Message()
            .attachment(new factory.ButtonTemplate()
                .text('This is a button template.')
                .buttons([
                    factory.Button.openUrl('https://haalamedia.com', `Visit HaalaMedia`),
                    factory.Button.postback('Postback Test', `Hello`)
                ]));

        bot.reply(message, msg);
    },

    quickreply: (bot, message, user) => {
        msg = new factory.Message()
            .text('This is a quick reply')
            .quickReplies([
                factory.QuickReply.email(),
                factory.QuickReply.location(),
                factory.QuickReply.phone(),
                factory.QuickReply.text(`Text button`, 'hello', 'https://i.imgur.com/W4ycGD1.jpg')
            ]);

        bot.reply(message, msg);
    },

    buy: (bot, message, user) => {
        msg = new factory.Message()
            .attachment(new factory.GenericTemplate('square')
                .elements([
                    new factory.GenericCard()
                        .title('Card #1')
                        .subtitle('Card 1 subtitle')
                        .image('https://i.imgur.com/W4ycGD1.jpg')
                        .buttons([
                            factory.Button.buy(`BUY_PRODUCT_1`)
                                   .product('Small', 19.99)
                                   .product('Large', 29.99)
                                   .merchant('HaalaMedia')
                        ])
                ]));

        bot.reply(message, msg);
    },

    list: (bot, message, user) => {
        msg = new factory.Message()
            .attachment(new factory.ListTemplate('large') // <compact|large>
                .sharable()
                .elements([
                    new factory.ListElement()
                        .title('List Item #1')
                        .subtitle('Subtitle')
                        .image('https://i.imgur.com/W4ycGD1.jpg')
                        .buttons([
                            factory.Button.openUrl('https://haalamedia.com', `Visit HaalaMedia`)
                        ])
                        .defaultAction('https://haalamedia.com'),
                    new factory.ListElement()
                        .title('Card #2')
                        .subtitle('Card 2 subtitle')
                        .image('https://i.imgur.com/GG1iFuF.png')
                        .buttons([
                            factory.Button.openUrl('https://chatsail.com', `Visit ChatSail`)
                        ])
                        .defaultAction('https://chatsail.com')
                ])
                .buttons([
                    factory.Button.postback('Postback Test', 'hello')
                ])
            );

        bot.reply(message, msg);
    },

    carousel: (bot, message, user) => {
        msg = new factory.Message()
            .attachment(new factory.GenericTemplate('square')
                .sharable()
                .elements([
                    new factory.GenericCard()
                        .title('Card #1')
                        .subtitle('Card 1 subtitle')
                        .image('https://i.imgur.com/W4ycGD1.jpg')
                        .buttons([
                            factory.Button.openUrl('https://haalamedia.com', `Visit HaalaMedia`)
                        ])
                        .defaultAction('https://haalamedia.com'),
                    new factory.GenericCard()
                        .title('Card #2')
                        .subtitle('Card 2 subtitle')
                        .image('https://i.imgur.com/GG1iFuF.png')
                        .buttons([
                            factory.Button.openUrl('https://chatsail.com', `Visit ChatSail`)
                        ])
                        .defaultAction('https://chatsail.com')
                ]));

        bot.reply(message, msg);
    },

    receipt: (bot, message, user) => {
        msg = new factory.Message()
            .attachment(new factory.Receipt()
                .orderId('123456')
                .orderUrl('https://haalamedia.com/orders/123456')
                .subtotal(75.00)
                .shipping(4.95)
                .tax(6.19)
                .total(56.14)
                .adjustment('New Customer Discount', 15)
                .adjustment('10% Off Coupon', 7.50)
                .paymentMethod('VISA 0123')
                .shipToName('Bill Gates')
                .shipToStreet1('1 Infinite Loop')
                .shipToStreet2('Ste. 123')
                .shipToCity('Cupertino')
                .shipToState('CA')
                .shipToPostal('95014')
                .shipToCountry('US')
                .merchant('HaalaMedia')
                .timestamp('1532718370') // Unix/Posix timestamp
                .elements([
                    new factory.ReceiptElement()
                        .title('Classic White T-Shirt')
                        .subtitle('100% Soft and Luxurious Cotton')
                        .price(25)
                        .quantity(1)
                        .image('https://i.imgur.com/W4ycGD1.jpg'),
                    new factory.ReceiptElement()
                        .title('Classic Grey T-Shirt')
                        .subtitle('100% Soft and Luxurious Cotton')
                        .price(25)
                        .quantity(2)
                        .image('https://i.imgur.com/GG1iFuF.png')
                ])
            );

        bot.reply(message, msg);
    },

}
