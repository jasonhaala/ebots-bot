'use strict';

const coreActions = require('./core');

let msg;

module.exports = {

    get_started_continue: (bot, message, user) => {
        console.log('=====================================')
        console.log('get_started_continue');
        console.log('=====================================')

        new Promise(function(resolve, reject) {
            msg = `It looks like we're going to have some fun`+ (user.firstName ? ', '+user.firstName : '') +'!';

            bot.reply(message, msg);

            setTimeout(function() {
                resolve();
            }, 1800);
        }).then(function(result) {
            return new Promise(function(resolve, reject) {
                if (typeof user.getUpdates === 'undefined') {
                    controller.trigger('updates_invite', [bot, message]);
                } else {
                    if (user.hasOnboarded) {
                        // Take the user directly to the main menu
                        coreActions.feature_carousel(bot, message, user);
                    } else {
                        // Take the user through the main menu onboarding presentation
                        module.exports.main_menu_onboarding(bot, message, user);
                    }
                }

                resolve();
            });
        });
    },

    main_menu_onboarding: (bot, message, user) => {
        console.log('=====================================')
        console.log('main_menu_onboarding');
        console.log('=====================================')

        new Promise(function(resolve, reject) {
            msg = `Now, let me show you a few things you can do here.`;

            bot.reply(message, msg);
            resolve();
        }).then(function(result) {
            return new Promise(function(resolve, reject) {
                msg = `Here's the main menu. You can scroll through and discover many of the features and information I've made available for you right here inside Facebook. 😊\r\n\r\nYou can return to these options any time by typing: *Menu*`

                setTimeout(function() {
                    bot.reply(message, msg);
                    resolve();
                }, 2000);
            });
        }).then(function(result) {
            return new Promise(function(resolve, reject) {
                user.hasOnboarded = true;
                // @todo Update user & cache

                bot.botkit.ebots.db.users.save(user, function(error, success) {
                    coreActions.feature_carousel(bot, message, user);

                    resolve();
                });
            });
        })
    }

}
