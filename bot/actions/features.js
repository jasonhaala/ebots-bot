'use strict';

let msg;

module.exports = {

    feature_carousel: (bot, message, user) => {
        console.log('=====================================')
        console.log('feature_carousel');
        console.log('=====================================')

        let factory = bot.botkit.factory;

        msg = new factory.Message()
            .attachment(new factory.GenericTemplate('square')
                .elements([
                    new factory.GenericCard()
                        .title('About Me')
                        .subtitle('Card 1 subtitle')
                        .image('https://images.chatfuel.com/bot/raw/be9173d8-c3f9-44c2-8ef9-82387c5da90b')
                        .buttons([
                            factory.Button.postback('About Me', 'about_me'),
                            factory.Button.postback('Ask Me Anything', 'faq')
                        ]),
                    new factory.GenericCard()
                        .title('My Galleries')
                        .subtitle('Card 1 subtitle')
                        .image('https://images.chatfuel.com/bot/raw/c1afe40e-dd60-4436-bdcd-57fb92da8df5')
                        .buttons([
                            factory.Button.postback('See Photos', 'photo_gallery'),
                            factory.Button.postback('Watch Videos', 'video_gallery'),
                        ]),
                    new factory.GenericCard()
                        .title('Meet & Greet')
                        .subtitle('Card 1 subtitle')
                        .image('https://images.chatfuel.com/bot/raw/ae7d4380-1e9f-4d99-b81d-e9b7d3389513')
                        .buttons([
                            factory.Button.postback(`Meet Me in Person`, 'meet_me'),
                            factory.Button.openUrl('https://verifiedcall.com/profiles/TanyaTate', `Call Me`),
                            factory.Button.openUrl('https://verifiedcall.com/profiles/TanyaTate', `Text Me`)
                        ]),
                    new factory.GenericCard()
                        .title('Follow & Share')
                        .subtitle('Card 1 subtitle')
                        .image('https://images.chatfuel.com/bot/raw/b8ac43a5-6cb7-46bc-a58d-25a6cbb2c684')
                        .buttons([
                            factory.Button.openUrl('https://fancentro.com/tanya_tate', `VIP Snapchat`),
                            factory.Button.openUrl('http://www.twitter.com/TanyaTate', `Follow on Twitter`),
                            factory.Button.share()
                        ]),
                    new factory.GenericCard()
                        .title('Official Merchandise')
                        .subtitle('Card 1 subtitle')
                        .image('https://images.chatfuel.com/bot/raw/74334c50-3100-425e-9c5c-c95822777422')
                        .buttons([
                            factory.Button.postback(`Shop Now`, 'shop')
                        ]),
                    new factory.GenericCard()
                        .title('Send a Gift')
                        .subtitle('Card 1 subtitle')
                        .image('https://images.chatfuel.com/bot/raw/0d8b4e28-5026-454d-9cee-20db7bcbb947')
                        .buttons([
                            factory.Button.openUrl('https://www.amazon.com/gp/registry/wishlist/19VX77HNU83ZJ/?ie=UTF8&tag=horrocom-20', `My Wishlist`)
                        ]),
                ])
            );

        bot.reply(message, msg);
    },

    photo_gallery: (bot, message, user) => {
        msg = 'Photo gallery...';

        bot.reply(message, msg);
    },

    video_gallery: (bot, message, user) => {
        msg = 'Video gallery...';

        bot.reply(message, msg);
    },

    meet_me: (bot, message, user) => {
        msg = 'Meet me...';

        bot.reply(message, msg);
    },

    about_me: (bot, message, user) => {
        msg = `I was born in Liverpool, UK, but am now living in Los Angeles.\r\n\r\nWhen I am not being an adult actress or directing XXX movies, I enjoy everything from superhero to the theatre, Disneyland theme parks and the gym. I enjoy reading and my boxer dog Millie. I support Liverpool football club and am a big WWE wrestling fan.\r\n\r\nKisses,\r\nTanya Tate xox`;

        bot.reply(message, msg);
    },

    faq: (bot, message, user) => {
        msg = `FAQ...`;

        bot.reply(message, msg);
    },

}
