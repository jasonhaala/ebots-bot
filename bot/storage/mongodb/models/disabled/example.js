const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const collection = 'example';

const ModelSchema = Schema({
    _id: Schema.Types.ObjectId,
    title:  String,
    message: Schema.Types.Mixed,
    stories: [{ type: Schema.Types.ObjectId, ref: 'Story' }],
    comments: [{
        body: String,
        date: Date
    }],
    date: { type: Date, default: Date.now },
    hidden: Boolean,
    meta: {
        votes: Number,
        favs:  Number
    }
}, {
    collection: collection
});


// Compound index
//ModelSchema.index({ channel: 1, userId: 1 });


module.exports = function(builder) {

    let model = mongoose.model(collection, ModelSchema);

    // Use the default builder methods or use a custom query function
    // Comment out any method to disable it
    return {

        /*
         Required methods
         */
        get: builder.get(model),

        save: builder.save(model),

        /*
         Optional methods
         */
        all: builder.all(model),

        delete: builder.delete(model),

        find: builder.find(model),

    };

};
